import { Link } from "react-router-dom";
import "./register.scss";
import { useState } from "react";
import axios from "axios";

const Register = () => {
    const [inputs, setInputs] = useState({
        username: "",
        email: "",
        password: "",
        name: "",
    });

    const [err, setErr] = useState(null);

    const handleChange = (e) => {
        setInputs((prev) => ({ ...prev, [e.target.name]: e.target.value }));
    };

    const handleClick = async (e) => {
        e.preventDefault();

        try {
            //Todo: Hardcoded url
            await axios.post("http://localhost:8800/api/auth/register", inputs);
        } catch (err) {
            setErr(err.response.data);
        }
    };

    return (
        <div className="register">
            <div className="card">
                <div className="left">
                    <h1>Social Book.</h1>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Libero cum, alias totam nunquam ipsa exercitationem
                        dignissimos, error nam, consequatur.
                    </p>
                    <span> Do you have an account?</span>
                    <Link to="/login">
                        <button>Login</button>
                    </Link>
                </div>
                <div className="right">
                    <h1>Register</h1>
                    <form>
                        <input
                            name="username"
                            onChange={handleChange}
                            type="text"
                            placeholder="Username"
                        />
                        <input
                            name="email"
                            onChange={handleChange}
                            type="email"
                            placeholder="Email"
                        />
                        <input
                            name="password"
                            onChange={handleChange}
                            type="password"
                            placeholder="Password"
                        />
                        <input
                            name="name"
                            onChange={handleChange}
                            type="text"
                            placeholder="Name"
                        />
                        {err && err}
                        <button onClick={handleClick}>Register</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default Register;
